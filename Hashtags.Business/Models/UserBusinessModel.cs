﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class UserBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }

        [Display (Name="Username")]
        public string Username { get; set; }
        [Display (Name="Password")]
        public string Password { get; set; }
        [Display (Name="FullName")]
        public string FullName { get; set; }
        [Display (Name="Bio")]
        public string Bio { get; set; }

        public List<Post> Posts { get; set; }
        public List<Hashtag> FollowedTags { get; set; }
        public List<Like> Likes { get; set; }
        public List<Comment> Comments { get; set; }
        public Picture Picture { get; set; }
        public List<Post> ChangingPosts { get; set; }

        public UserBusinessModel()
        {
            Posts = new List<Post>();
            ChangingPosts = new List<Post>();
            FollowedTags = new List<Hashtag>();
            Likes = new List<Like>();
            Comments = new List<Comment>();
            Timestamp = DateTime.Now;
        }

        public static UserBusinessModel FromDatabaseModel(User user)
        {
            return new UserBusinessModel()
            {
                Id = DAUser.GetStringId(user),
                Timestamp = DAUser.GetDateTime(user),
                Username = user.Username,
                Password = user.Password,
                FullName = user.FullName,
                Bio = user.Bio,
                Picture = user.Picture,
                Posts = user.Posts,
                FollowedTags = user.FollowedTags,
                Likes = user.Likes,
                Comments = user.Comments,
            };
        }

        public static User ToDatabaseModel(UserBusinessModel user)
        {
            return new User()
            {
                Id = DAUser.GetObjectId(user.Id),
                Timestamp = DAUser.GetBsonDateTime(user.Timestamp),
                Username = user.Username,
                Password = user.Password,
                FullName = user.FullName,
                Bio = user.Bio,
                Picture = user.Picture,
                Posts = user.Posts,
                FollowedTags = user.FollowedTags,
                Likes = user.Likes,
                Comments = user.Comments,
            };
        }
    }
}
