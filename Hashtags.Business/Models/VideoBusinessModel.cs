﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class VideoBusinessModel : AttachmentBusinessModel
    {
        public VideoBusinessModel()
        {
            Type = Database.Enums.AttachmentType.Video;
        }

        public static VideoBusinessModel FromDatabaseModel(Video pic)
        {
            return new VideoBusinessModel()
            {
                Id = DAVideo.GetStringId(pic),
                Timestamp = DAVideo.GetDateTime(pic),
                OwnerId = pic.OwnerId.ToString(),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }

        public static Video ToDatabaseModel(VideoBusinessModel pic)
        {
            return new Video()
            {
                Id = DAVideo.GetObjectId(pic.Id),
                Timestamp = DAVideo.GetBsonDateTime(pic.Timestamp),
                OwnerId = DAVideo.GetObjectId(pic.OwnerId),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }
    }
}
