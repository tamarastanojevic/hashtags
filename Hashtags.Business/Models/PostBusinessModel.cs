﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class PostBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }
        
        public string OwnerId { get; set; }
        [Display(Name = "OwnerUsername")]
        public string OwnerUsername { get; set; }
        [Display(Name = "Body")]
        public string Body { get; set; }

        public List<Comment> Comments { get; set; }
        public List<Like> Likes { get; set; }
        public List<Hashtag> Hashtags { get; set; }
        public Attachment Attachment { get; set; }

        public PostBusinessModel()
        {
            Comments = new List<Comment>();
            Hashtags = new List<Hashtag>();
            Likes = new List<Like>();
            Attachment = new Attachment();
            Timestamp = DateTime.Now;
        }

        public static PostBusinessModel FromDatabaseModel(Post post)
        {
            return new PostBusinessModel()
            {
                Id = DAPost.GetStringId(post),
                Timestamp = DAPost.GetDateTime(post),
                OwnerId = post.OwnerId.ToString(),
                OwnerUsername = post.OwnerUsername,
                Body =post.Body,
                Attachment = post.Attachment,
                Likes = post.Likes,
                Hashtags = post.Hashtags,
                Comments = post.Comments
            };
        }

        public static Post ToDatabaseModel(PostBusinessModel post)
        {
            return new Post()
            {
                Id = DAPost.GetObjectId(post.Id),
                Timestamp = DAPost.GetBsonDateTime(post.Timestamp),
                OwnerId = DAPost.GetObjectId(post.OwnerId),
                OwnerUsername = post.OwnerUsername,
                Body = post.Body,
                Attachment = post.Attachment,
                Likes = post.Likes,
                Hashtags = post.Hashtags,
                Comments = post.Comments
            };
        }
    }
}
