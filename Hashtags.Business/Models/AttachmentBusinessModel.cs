﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Enums;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class AttachmentBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }

        public string OwnerId { get; set; }
        [Display(Name = "Owner Type")]
        public OwnerType OwnerType{ get; set; }

        public byte[] Data { get; set; }
        [Display(Name = "Type")]
        public AttachmentType Type { get; set; }

        public AttachmentBusinessModel()
        {
            Timestamp = DateTime.Now;
        }

        public static AttachmentBusinessModel FromDatabaseModel(Attachment attachment)
        {
            return new AttachmentBusinessModel()
            {
                Id = DAAttachment.GetStringId(attachment),
                Timestamp = DAAttachment.GetDateTime(attachment),
                OwnerId = attachment.OwnerId.ToString(),
                OwnerType = attachment.OwnerType,
                Data = attachment.Data,
                Type = attachment.Type
            };
        }

        public static Attachment ToDatabaseModel(AttachmentBusinessModel attachment)
        {
            return new Attachment()
            {
                Id = DAAttachment.GetObjectId(attachment.Id),
                Timestamp = DAAttachment.GetBsonDateTime(attachment.Timestamp),
                OwnerId = DAAttachment.GetObjectId(attachment.OwnerId),
                OwnerType = attachment.OwnerType,
                Data = attachment.Data,
                Type = attachment.Type
            };
        }
    }
}
