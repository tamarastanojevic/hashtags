﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class PdfBusinessModel : AttachmentBusinessModel
    {
        public PdfBusinessModel()
        {
            Type = Database.Enums.AttachmentType.Pdf;
        }

        public static PdfBusinessModel FromDatabaseModel(Pdf pic)
        {
            return new PdfBusinessModel()
            {
                Id = DAPdf.GetStringId(pic),
                Timestamp = DAPdf.GetDateTime(pic),
                OwnerId = pic.OwnerId.ToString(),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }

        public static Pdf ToDatabaseModel(PdfBusinessModel pic)
        {
            return new Pdf()
            {
                Id = DAPdf.GetObjectId(pic.Id),
                Timestamp = DAPdf.GetBsonDateTime(pic.Timestamp),
                OwnerId = DAPdf.GetObjectId(pic.OwnerId),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }
    }
}
