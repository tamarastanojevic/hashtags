﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class PictureBusinessModel : AttachmentBusinessModel
    {
        public PictureBusinessModel()
        {
            Type = Database.Enums.AttachmentType.Picture;
        }

        public static PictureBusinessModel FromDatabaseModel(Picture pic)
        {
            return new PictureBusinessModel()
            {
                Id = DAPicture.GetStringId(pic),
                Timestamp = DAPicture.GetDateTime(pic),
                OwnerId = pic.OwnerId.ToString(),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }

        public static Picture ToDatabaseModel(PictureBusinessModel pic)
        {
            return new Picture()
            {
                Id = DAPicture.GetObjectId(pic.Id),
                Timestamp = DAPicture.GetBsonDateTime(pic.Timestamp),
                OwnerId = DAPicture.GetObjectId(pic.OwnerId),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }
    }
}
