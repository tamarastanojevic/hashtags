﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class AudioBusinessModel : AttachmentBusinessModel
    {
        public AudioBusinessModel()
        {
            Type = Database.Enums.AttachmentType.Audio;
        }

        public static AudioBusinessModel FromDatabaseModel(Audio pic)
        {
            return new AudioBusinessModel()
            {
                Id = DAAudio.GetStringId(pic),
                Timestamp = DAAudio.GetDateTime(pic),
                OwnerId = pic.OwnerId.ToString(),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }

        public static Audio ToDatabaseModel(AudioBusinessModel pic)
        {
            return new Audio()
            {
                Id = DAAudio.GetObjectId(pic.Id),
                Timestamp = DAAudio.GetBsonDateTime(pic.Timestamp),
                OwnerId = DAAudio.GetObjectId(pic.OwnerId),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }
    }
}
