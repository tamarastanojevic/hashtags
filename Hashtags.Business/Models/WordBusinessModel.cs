﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class WordBusinessModel : AttachmentBusinessModel
    {
        public WordBusinessModel()
        {
            Type = Database.Enums.AttachmentType.Word;
        }

        public static WordBusinessModel FromDatabaseModel(Word pic)
        {
            return new WordBusinessModel()
            {
                Id = DAWord.GetStringId(pic),
                Timestamp = DAWord.GetDateTime(pic),
                OwnerId = pic.OwnerId.ToString(),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }

        public static Word ToDatabaseModel(WordBusinessModel pic)
        {
            return new Word()
            {
                Id = DAWord.GetObjectId(pic.Id),
                Timestamp = DAWord.GetBsonDateTime(pic.Timestamp),
                OwnerId = DAWord.GetObjectId(pic.OwnerId),
                OwnerType = pic.OwnerType,
                Data = pic.Data,
                Type = pic.Type
            };
        }
    }
}
