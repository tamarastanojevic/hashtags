﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class HashtagBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
        
        public List<Post> Posts { get; set; }
        public List<User> FollowingUsers { get; set; }

        public HashtagBusinessModel()
        {
            Posts = new List<Post>();
            FollowingUsers = new List<User>();
            Timestamp = DateTime.Now;
        }

        public static HashtagBusinessModel FromDatabaseModel(Hashtag hash)
        {
            return new HashtagBusinessModel()
            {
                Id = DAHashtag.GetStringId(hash),
                Timestamp = DAHashtag.GetDateTime(hash),
                Name = hash.Name,
                Posts = hash.Posts,
                FollowingUsers = hash.FollowingUsers
            };
        }

        public static Hashtag ToDatabaseModel(HashtagBusinessModel hash)
        {
            return new Hashtag()
            {
                Id = DAHashtag.GetObjectId(hash.Id),
                Timestamp = DAHashtag.GetBsonDateTime(hash.Timestamp),
                Name = hash.Name,
                Posts = hash.Posts,
                FollowingUsers = hash.FollowingUsers
            };
        }
    }
}
