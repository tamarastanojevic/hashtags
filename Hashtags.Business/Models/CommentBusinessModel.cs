﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class CommentBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }

        public string OwnerId { get; set; }
        [Display(Name = "OwnerUsername")]
        public string OwnerUsername { get; set; }
        [Display(Name = "Text")]
        public string Text { get; set; }
        public Attachment Attachment { get; set; }

        public CommentBusinessModel()
        {
            Attachment = new Attachment();
            Timestamp = DateTime.Now;
        }

        public static CommentBusinessModel FromDatabaseModel(Comment comment)
        {
            return new CommentBusinessModel()
            {
                Id = DAComment.GetStringId(comment),
                Timestamp = DAComment.GetDateTime(comment),
                OwnerId = comment.OwnerId.ToString(),
                OwnerUsername = comment.OwnerUsername,
                Text = comment.Text,
                Attachment = comment.Attachment
            };
        }

        public static Comment ToDatabaseModel(CommentBusinessModel comment)
        {
            return new Comment()
            {
                Id = DAComment.GetObjectId(comment.Id),
                Timestamp = DAComment.GetBsonDateTime(comment.Timestamp),
                OwnerId = DAComment.GetObjectId(comment.OwnerId),
                OwnerUsername = comment.OwnerUsername,
                Text = comment.Text,
                Attachment = comment.Attachment
            };
        }
    }
}
