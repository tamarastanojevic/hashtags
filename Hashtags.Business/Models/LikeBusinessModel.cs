﻿using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.Models
{
    public class LikeBusinessModel
    {
        public string Id { get; set; }
        public DateTime Timestamp { get; set; }

        public string UserOwnerId { get; set; }
        [Display(Name = "Owner Username")]
        public string OwnerUsername { get; set; }
        public string PostOwnerId { get; set; }
   
        public LikeBusinessModel()
        {
            Timestamp = DateTime.Now;
        }

        public static LikeBusinessModel FromDatabaseModel(Like like)
        {
            return new LikeBusinessModel()
            {
                Id = DALike.GetStringId(like),
                Timestamp = DALike.GetDateTime(like),
                UserOwnerId = like.UserOwnerId.ToString(),
                OwnerUsername = like.OwnerUsername,
                PostOwnerId = like.PostOwnerId.ToString()
            };
        }

        public static Like ToDatabaseModel(LikeBusinessModel like)
        {
            return new Like()
            {
                Id = DALike.GetObjectId(like.Id),
                Timestamp = DALike.GetBsonDateTime(like.Timestamp),
                UserOwnerId = DALike.GetObjectId(like.UserOwnerId),
                OwnerUsername = like.OwnerUsername,
                PostOwnerId = DALike.GetObjectId(like.PostOwnerId)
            };
        }
    }
}
