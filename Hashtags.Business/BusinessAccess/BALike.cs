﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BALike
    {
        public static LikeBusinessModel GetLikeById(string id)
        {
            return LikeBusinessModel.FromDatabaseModel(DALike.GetLikeById(id));
        }

        public static List<LikeBusinessModel> GetAllLikesFromPostId(string id)
        {
            List<LikeBusinessModel> newlist = new List<LikeBusinessModel>();
            var list = DALike.GetAllLikesFromPostId(id);

            foreach (var el in list)
            {
                newlist.Add(LikeBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<LikeBusinessModel> GetAllLikesFromUserId(string id)
        {
            List<LikeBusinessModel> newlist = new List<LikeBusinessModel>();
            var list = DALike.GetAllLikesFromUserId(id);

            foreach (var el in list)
            {
                newlist.Add(LikeBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<LikeBusinessModel> GetAllLikesFromUsersUsername(string username)
        {
            List<LikeBusinessModel> newlist = new List<LikeBusinessModel>();
            var list = DALike.GetAllLikesFromUsersUsername(username);

            foreach (var el in list)
            {
                newlist.Add(LikeBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static void CreateLike(LikeBusinessModel like)
        {
            var model = LikeBusinessModel.ToDatabaseModel(like);
            DALike.CreateLike(model.UserOwnerId, model.OwnerUsername, model.PostOwnerId);
        }

        public static void DeleteLikeById(string id)
        {
            DALike.DeleteLikeById(id);
        }

    }
}
