﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAVideo
    {
        public static VideoBusinessModel GetVideoById(string id)
        {
            return VideoBusinessModel.FromDatabaseModel(DAVideo.GetVideoById(id));
        }

        public static void CreateVideo(VideoBusinessModel Video)
        {
            var model = VideoBusinessModel.ToDatabaseModel(Video);
            DAVideo.CreateVideo(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeleteVideoById(string id)
        {
            DAVideo.DeleteVideoById(id);
        }
    }
}
