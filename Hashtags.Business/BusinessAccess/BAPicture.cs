﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAPicture
    {
        public static PictureBusinessModel GetPictureById(string id)
        {
            return PictureBusinessModel.FromDatabaseModel(DAPicture.GetPictureById(id));
        }

        public static void CreatePicture(PictureBusinessModel Picture)
        {
            var model = PictureBusinessModel.ToDatabaseModel(Picture);
            DAPicture.CreatePicture(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeletePictureById(string id)
        {
            DAPicture.DeletePictureById(id);
        }
    }
}
