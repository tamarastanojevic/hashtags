﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAAudio
    {
        public static AudioBusinessModel GetAudioById(string id)
        {
            return AudioBusinessModel.FromDatabaseModel(DAAudio.GetAudioById(id));
        }

        public static void CreateAudio(AudioBusinessModel audio)
        {
            var model = AudioBusinessModel.ToDatabaseModel(audio);
            DAAudio.CreateAudio(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeleteAudioById(string id)
        {
            DAAudio.DeleteAudioById(id);
        }

    }
}
