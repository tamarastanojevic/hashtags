﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAWord
    {
        public static WordBusinessModel GetWordById(string id)
        {
            return WordBusinessModel.FromDatabaseModel(DAWord.GetWordById(id));
        }

        public static void CreateWord(WordBusinessModel Word)
        {
            var model = WordBusinessModel.ToDatabaseModel(Word);
            DAWord.CreateWord(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeleteWordById(string id)
        {
            DAWord.DeleteWordById(id);
        }
    }
}
