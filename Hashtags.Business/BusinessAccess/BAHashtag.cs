﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAHashtag
    {
        public static HashtagBusinessModel GetHashtagById(string id)
        {
            return HashtagBusinessModel.FromDatabaseModel(DAHashtag.GetHashtagById(id));
        }

        public static List<PostBusinessModel> GetAllHashtagPosts(string id)
        {
            List<PostBusinessModel> newlist = new List<PostBusinessModel>();
            var list = DAHashtag.GetAllHashtagPosts(id);

            foreach(var el in list)
            {
                newlist.Add(PostBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<UserBusinessModel> GetAllHashtagFollowingUsers(string id)
        {
            List<UserBusinessModel> newlist = new List<UserBusinessModel>();
            var list = DAHashtag.GetAllHashtagFollowingUsers(id);

            foreach (var el in list)
            {
                newlist.Add(UserBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }
        public static void CreateHashtag(HashtagBusinessModel hashtag)
        {
            var model = HashtagBusinessModel.ToDatabaseModel(hashtag);
            DAHashtag.CreateHashtag(model.Name, model.Posts);
        }

        public static void DeleteHashtagById(string id)
        {
            DAHashtag.DeleteHashtagById(id);
        }

    }
}
