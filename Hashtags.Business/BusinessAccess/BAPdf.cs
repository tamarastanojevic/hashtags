﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAPdf
    {
        public static PdfBusinessModel GetPdfById(string id)
        {
            return PdfBusinessModel.FromDatabaseModel(DAPdf.GetPdfById(id));
        }

        public static void CreatePdf(PdfBusinessModel pdf)
        {
            var model = PdfBusinessModel.ToDatabaseModel(pdf);
            DAPdf.CreatePdf(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeletePdfById(string id)
        {
            DAPdf.DeletePdfById(id);
        }

    }
}
