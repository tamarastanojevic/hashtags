﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAComment
    {
        public static CommentBusinessModel GetCommentById(string id)
        {
            return CommentBusinessModel.FromDatabaseModel(DAComment.GetCommentById(id));
        }

        public static PictureBusinessModel GetCommentPicture(string id)
        {
            var comment = CommentBusinessModel.FromDatabaseModel(DAComment.GetCommentById(id));

            PictureBusinessModel picture = new PictureBusinessModel();
            picture.Data = comment.Attachment.Data;
            picture.Id = comment.Attachment.Id.ToString();
            picture.OwnerId = comment.Attachment.OwnerId.ToString();
            picture.OwnerType = comment.Attachment.OwnerType;
            picture.Timestamp = DAComment.GetDateTime(comment.Attachment);
            picture.Type = comment.Attachment.Type;

            return picture;
        }

        public static void CreateComment(CommentBusinessModel comment, string ownerId, string ownerUsername, string text)
        {
            var model = CommentBusinessModel.ToDatabaseModel(comment);
            DAComment.CreateComment(model.OwnerId.ToString(), model.OwnerUsername, model.Text);
        }

        public static void DeleteCommentById(string id)
        {
            DAComment.DeleteCommentById(id);
        }

    }
}
