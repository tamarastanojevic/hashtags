﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using Hashtags.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAPost
    {
        public static PostBusinessModel GetPostById(string id)
        {
            return PostBusinessModel.FromDatabaseModel(DAPost.GetPostById(id));
        }

        public static List<PostBusinessModel> GetAllPostsFromUsersId(string id)
        {
            List<PostBusinessModel> newlist = new List<PostBusinessModel>();
            var list = DAPost.GetAllPostsFromUsersId(id);

            foreach (var el in list)
            {
                newlist.Add(PostBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<PostBusinessModel> GetAllPostsFromUsersUsername(string username)
        {
            List<PostBusinessModel> newlist = new List<PostBusinessModel>();
            var list = DAPost.GetAllPostsFromUsersUsername(username);

            foreach (var el in list)
            {
                newlist.Add(PostBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<HashtagBusinessModel> GetAllPostHashtags(string id)
        {
            List<HashtagBusinessModel> newlist = new List<HashtagBusinessModel>();
            var list = DAPost.GetAllPostHashtags(id);

            foreach (var el in list)
            {
                newlist.Add(HashtagBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<CommentBusinessModel> GetAllPostComments(string id)
        {
            List<CommentBusinessModel> newlist = new List<CommentBusinessModel>();
            var list = DAPost.GetAllPostComments(id);

            foreach (var el in list)
            {
                newlist.Add(CommentBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<LikeBusinessModel> GetAllPostLikes(string id)
        {
            List<LikeBusinessModel> newlist = new List<LikeBusinessModel>();
            var list = DAPost.GetAllPostLikes(id);

            foreach (var el in list)
            {
                newlist.Add(LikeBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static PictureBusinessModel GetPostPicture(string id)
        {
            var post = PostBusinessModel.FromDatabaseModel(DAPost.GetPostById(id));

            PictureBusinessModel picture = new PictureBusinessModel();
            picture.Data = post.Attachment.Data;
            picture.Id = post.Attachment.Id.ToString();
            picture.OwnerId = post.Attachment.OwnerId.ToString();
            picture.OwnerType = post.Attachment.OwnerType;
            picture.Timestamp = DAPost.GetDateTime(post.Attachment);
            picture.Type = post.Attachment.Type;

            return picture;
        }

        public static void CreatePost(PostBusinessModel post)
        {
            var model = PostBusinessModel.ToDatabaseModel(post);
            DAPost.CreatePost(model.OwnerId, model.OwnerUsername, model.Body, model.Link, model.EditState, model.Hashtags);
        }

        public static void UpdatePostBodyById(string id, string body)
        {
            DAPost.UpdatePostBodyById(id, body);
        }

        public static void UpdatePostLinkById(string id, string link)
        {
            DAPost.UpdatePostLinkById(id, link);
        }

        public static void UpdatePostEditStateById(string id, string editState)
        {
            DAPost.UpdatePostEditStateById(id, editState);
        }

        public static void UpdatePostHashtagsById(string id, List<HashtagBusinessModel> hashtags)
        {
            List<Hashtag> newlist = new List<Hashtag>();
            foreach(var el in hashtags)
            {
                newlist.Add(HashtagBusinessModel.ToDatabaseModel(el));
            }
            DAPost.UpdatePostHashtagsById(id, newlist);
        }

        public static void DeletePostById(string id)
        {
            DAPost.DeletePostById(id);
        }
    }
}
