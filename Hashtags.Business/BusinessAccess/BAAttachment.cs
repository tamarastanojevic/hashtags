﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAAttachment
    {
        public static AttachmentBusinessModel GetAttachmentById(string id)
        {
            return AttachmentBusinessModel.FromDatabaseModel(DAAttachment.GetAttachmentById(id));
        }

        public static void CreateAttachment(AttachmentBusinessModel attachment)
        {
            var model = AttachmentBusinessModel.ToDatabaseModel(attachment);
            DAAttachment.CreateAttachment(model.OwnerId.ToString(), model.OwnerType, model.Data);
        }

        public static void DeleteAttachmentById(string id)
        {
            DAAttachment.DeleteAttachmentById(id);
        }
    }
}
