﻿using Hashtags.Business.Models;
using Hashtags.Database.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Business.BusinessAccess
{
    public class BAUser
    {
        public static UserBusinessModel GetUserById(string id)
        {
            return UserBusinessModel.FromDatabaseModel(DAUser.GetUserById(id));
        }

        public static UserBusinessModel GetUserByUsername(string username)
        {
            return UserBusinessModel.FromDatabaseModel(DAUser.GetUserByUsername(username));
        }

        public static List<UserBusinessModel> GetAllUsers()
        {
            List<UserBusinessModel> newlist = new List<UserBusinessModel>();
            var list = DAUser.GetAllUsers();

            foreach (var el in list)
            {
                newlist.Add(UserBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<HashtagBusinessModel> GetAllUserFollowedHashtags(string id)
        {
            List<HashtagBusinessModel> newlist = new List<HashtagBusinessModel>();
            var list = DAUser.GetAllUserFollowedHashtags(id);

            foreach (var el in list)
            {
                newlist.Add(HashtagBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<PostBusinessModel> GetAllUserPosts(string id)
        {
            List<PostBusinessModel> newlist = new List<PostBusinessModel>();
            var list = DAUser.GetAllUserPosts(id);

            foreach (var el in list)
            {
                newlist.Add(PostBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<LikeBusinessModel> GetAllUserLikes(string id)
        {
            List<LikeBusinessModel> newlist = new List<LikeBusinessModel>();
            var list = DAUser.GetAllUserLikes(id);

            foreach (var el in list)
            {
                newlist.Add(LikeBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static List<CommentBusinessModel> GetAllUserComments(string id)
        {
            List<CommentBusinessModel> newlist = new List<CommentBusinessModel>();
            var list = DAUser.GetAllUserComments(id);

            foreach (var el in list)
            {
                newlist.Add(CommentBusinessModel.FromDatabaseModel(el));
            }

            return newlist;
        }

        public static PictureBusinessModel GetUserPicture(string id)
        {
            var user = UserBusinessModel.FromDatabaseModel(DAUser.GetUserById(id));

            PictureBusinessModel picture = new PictureBusinessModel();
            picture.Data = user.Picture.Data;
            picture.Id = user.Picture.Id.ToString();
            picture.OwnerId = user.Picture.OwnerId.ToString();
            picture.OwnerType = user.Picture.OwnerType;
            picture.Timestamp = DAUser.GetDateTime(user.Picture);
            picture.Type = user.Picture.Type;

            return picture;
        }

        public static bool CreateUser(UserBusinessModel user)
        {
            var model = UserBusinessModel.ToDatabaseModel(user);
            return DAUser.CreateUser(model.Username, model.Password, model.FullName, model.Bio, model.Picture);
        }

        public static void UpdateUserById(string id, string password, string fullName, string bio, PictureBusinessModel picture)
        {
            DAUser.UpdateUserById(id, password, fullName, bio, PictureBusinessModel.ToDatabaseModel(picture));
        }

        public static void UpdateUserByUsername(string username, string password, string fullName, string bio, PictureBusinessModel picture)
        {
            DAUser.UpdateUserByUsername(username, password, fullName, bio, PictureBusinessModel.ToDatabaseModel(picture));
        }

        public static void DeleteUserById(string id)
        {
            DAUser.DeleteUserById(id);
        }

        public static void DeleteUserByUsername(string username)
        {
            DAUser.DeleteUserByUsername(username);
        }

    }
}
