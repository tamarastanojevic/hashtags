﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web;

namespace Hashtags.Database.Helpers
{
    public class MongoContext
    {
        private static MongoContext instance = null;
        private MongoClient client;
        public IMongoDatabase database;
       
        private MongoContext()
        {
            client = new MongoClient(ConnectionHelper.CONNECTION_STRING);
            database = client.GetDatabase(ConnectionHelper.DATABASE_NAME);
        }

        public static MongoContext Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MongoContext();
                }
                return instance;
            }
        }
        
    }
}
