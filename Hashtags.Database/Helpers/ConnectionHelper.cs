﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Helpers
{
    public static class ConnectionHelper
    {
        public static string DATABASE_NAME = "HashtagsDatabase";
        public static string CONNECTION_STRING = "mongodb://127.0.0.1:27017";

        public static string USER_COLLECTION = "User";
        public static string HASHTAG_COLLECTION = "Hashtag";
        public static string POST_COLLECTION = "Post";
        public static string COMMENT_COLLECTION = "Comment";
        public static string LIKE_COLLECTION = "Like";
        public static string ATTACHMENT_COLLECTION = "Attachment";
    }
}
