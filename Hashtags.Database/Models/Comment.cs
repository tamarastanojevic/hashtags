﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Comment
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("OwnerId")]
        public ObjectId OwnerId { get; set; }
        [BsonElement("OwnerUsername")]
        public string OwnerUsername { get; set; }

        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }
        [BsonElement("Text")]
        public string Text { get; set; }
        [BsonElement("Attachment")]
        public Attachment Attachment { get; set; }

        public Comment()
        {
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}
