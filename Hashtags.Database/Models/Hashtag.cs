﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Hashtag
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("Posts")]
        public List<Post> Posts { get; set; }
        [BsonElement("FollowingUsers")]
        public List<User> FollowingUsers { get; set; }

        public Hashtag()
        {
            Posts = new List<Post>();
            FollowingUsers = new List<User>();
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}
