﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Post
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }

        [BsonElement("OwnerId")]
        public ObjectId OwnerId { get; set; }
        [BsonElement("OwnerUsername")]
        public string OwnerUsername { get; set; }

        [BsonElement("Body")]
        public string Body { get; set; }
        [BsonElement("Link")]
        public string Link { get; set; }
        [BsonElement("EditState")]
        public string EditState { get; set; }

        [BsonElement("Comments")]
        public List<Comment> Comments { get; set; }
        [BsonElement("Likes")]
        public List<Like> Likes { get; set; }
        [BsonElement("Hashtags")]
        public List<Hashtag> Hashtags { get; set; }
        [BsonElement("Attachment")]
        public Attachment Attachment { get; set; }
        [BsonElement("ChangingUsers")]
        public List<User> ChangingUsers { get; set; }


        public Post()
        {
            Likes = new List<Like>();
            Comments = new List<Comment>();
            Hashtags = new List<Hashtag>();
            ChangingUsers = new List<User>();
            Timestamp = new BsonDateTime(DateTime.Now);
            EditState = String.Empty;
            Link = String.Empty;
        }
    }
}
