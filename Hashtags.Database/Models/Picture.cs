﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Picture : Attachment
    {
        public Picture()
        {
            Type = Enums.AttachmentType.Picture;
        }
    }
}
