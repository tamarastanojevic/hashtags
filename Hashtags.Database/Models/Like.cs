﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Like
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }

        [BsonElement("UserOwnerId")]
        public ObjectId UserOwnerId { get; set; }
        [BsonElement("OwnerUsername")]
        public string OwnerUsername { get; set; }

        [BsonElement("PostOwnerId")]
        public ObjectId PostOwnerId { get; set; }

        public Like()
        {
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}
