﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Pdf : Attachment
    {
        public Pdf()
        {
            Type = Enums.AttachmentType.Pdf;
        }
    }
}
