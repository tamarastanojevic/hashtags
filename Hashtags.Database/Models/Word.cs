﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Word : Attachment
    {
        public Word()
        {
            Type = Enums.AttachmentType.Word;
        }
    }
}
