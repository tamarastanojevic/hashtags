﻿using Hashtags.Database.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Attachment
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }

        [BsonElement("OwnerId")]
        public ObjectId OwnerId { get; set; }
        [BsonElement("OwnerType")]
        public OwnerType OwnerType { get; set; }

        [BsonElement("Data")]
        public byte[] Data { get; set; }
        [BsonElement("Type")]
        public AttachmentType Type { get; set; }

        public Attachment()
        {
            Timestamp = new BsonDateTime(DateTime.Now);
            Data = new byte[20480];
        }
    }
}
