﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class User
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }

        [BsonElement("Username")]
        public string Username { get; set; }
        [BsonElement("Password")]
        public string Password { get; set; }
        [BsonElement("FullName")]
        public string FullName { get; set; }
        [BsonElement("Bio")]
        public string Bio { get; set; }

        [BsonElement("Posts")]
        public List<Post> Posts { get; set; }
        [BsonElement("FollowedTags")]
        public List<Hashtag> FollowedTags { get; set; }
        [BsonElement("Likes")]
        public List<Like> Likes { get; set; }
        [BsonElement("Comments")]
        public List<Comment> Comments { get; set; }
        [BsonElement("Picture")]
        public Picture Picture { get; set; }
        [BsonElement("ChangingPosts")]
        public List<Post> ChangingPosts { get; set; }


        public User()
        {
            Posts = new List<Post>();
            FollowedTags = new List<Hashtag>();
            Likes = new List<Like>();
            Comments = new List<Comment>();
            ChangingPosts = new List<Post>();
            Timestamp = new BsonDateTime(DateTime.Now);
        }
    }
}
