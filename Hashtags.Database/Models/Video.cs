﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Models
{
    public class Video : Attachment
    {
        public Video()
        {
            Type = Enums.AttachmentType.Video;
        }
    }
}
