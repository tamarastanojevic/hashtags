﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.Enums
{
    public enum AttachmentType
    {
        Picture = 0,
        Pdf = 1,
        Word = 2,
        Audio = 3,
        Video = 4
    }

    public enum OwnerType
    {
        User = 0,
        Post = 1,
        Comment = 2
    }
}
