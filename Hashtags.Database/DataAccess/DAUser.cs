﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Hashtags.Database.DataAccess
{
    public class DAUser
    {
        public static User GetUserById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<User>(result);
        }

        public static User GetUserByUsername(string username)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("Username", username);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<User>(result);
        }

        public static List<User> GetAllUsers()
        {
            List<User> returnList = new List<User>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Empty;
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<User>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static List<Hashtag> GetAllUserFollowedHashtags(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var user = BsonSerializer.Deserialize<User>(result.ToBsonDocument());

            return user.FollowedTags;
        }

        public static List<Post> GetAllUserPosts(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var user = BsonSerializer.Deserialize<User>(result.ToBsonDocument());

            return user.Posts;
        }

        public static List<Like> GetAllUserLikes(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var user = BsonSerializer.Deserialize<User>(result.ToBsonDocument());

            return user.Likes;
        }

        public static List<Comment> GetAllUserComments(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var user = BsonSerializer.Deserialize<User>(result.ToBsonDocument());

            return user.Comments;
        }

        public static Picture GetUserPicture(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var user = BsonSerializer.Deserialize<User>(result.ToBsonDocument());

            return user.Picture;
        }

        public static bool CreateUser(string username, string password, string fullName, string bio, Picture picture)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);
            var query = Builders<BsonDocument>.Filter.Eq("Username", username);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault();

            if (result == null)
            {
                User user = new User()
                {
                    Username = username,
                    Password = password,
                    FullName = fullName,
                    Bio = bio,
                    Picture = picture
                };

                var item = user.ToBsonDocument<User>();
                collection.InsertOne(item);

                return true;
            }
            else
            {
                return false;
            }
        }

        public static void UpdateUserById(string id, string password, string fullName, string bio, Picture picture)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));

            var update = Builders<BsonDocument>.Update.Set("Password", password);
            var result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("FullName", fullName);
            result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Bio", bio);
            result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Picture", picture);
            result = collection.UpdateOneAsync(filter, update);
        }

        public static void UpdateUserByUsername(string username, string password, string fullName, string bio, Picture picture)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("Username", username);

            var update = Builders<BsonDocument>.Update.Set("Password", password);
            var result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("FullName", fullName);
            result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Bio", bio);
            result = collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Picture", picture);
            result = collection.UpdateOneAsync(filter, update);
        }


        public static void DeleteUserById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static void DeleteUserByUsername(string username)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.USER_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("Username", username);
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(User user)
        {
            return user.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(User user)
        {
            return user.Timestamp.AsDateTime;
        }

        public static DateTime GetDateTime(Attachment user)
        {
            return user.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }

}
