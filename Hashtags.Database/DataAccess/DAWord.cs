﻿using Hashtags.Database.Enums;
using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DAWord
    {
        public static Word GetWordById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Word>(result);
        }

        public static void CreateWord(string ownerId, OwnerType ownerType, byte[] data)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            Word pic = new Word()
            {
                OwnerId = new ObjectId(ownerId),
                Data = data,
                OwnerType = ownerType,
                Type = Enums.AttachmentType.Word
            };

            var item = pic.ToBsonDocument<Word>();
            collection.InsertOne(item);
        }

        public static void DeleteWordById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Word pic)
        {
            return pic.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Word pic)
        {
            return pic.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
