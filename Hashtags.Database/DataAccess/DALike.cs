﻿using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DALike
    {
        public static Like GetLikeById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Like>(result);
        }

        public static List<Like> GetAllLikesFromPostId(string id)
        {
            List<Like> returnList = new List<Like>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("PostOwnerId", id);
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<Like>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static List<Like> GetAllLikesFromUserId(string id)
        {
            List<Like> returnList = new List<Like>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("UserOwnerId", id);
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<Like>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static List<Like> GetAllLikesFromUsersUsername(string username)
        {
            List<Like> returnList = new List<Like>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("OwnerUsername", username);
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<Like>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static void CreateLike(ObjectId userOwnerId, string userOwnerUsername, ObjectId postOwnerId)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            Like like = new Like()
            {
                UserOwnerId=userOwnerId,
                OwnerUsername = userOwnerUsername,
                PostOwnerId=postOwnerId
            };

            var item = like.ToBsonDocument<Like>();
            collection.InsertOne(item);
        }

        public static void DeleteLikeById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.LIKE_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Like like)
        {
            return like.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Like like)
        {
            return like.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
