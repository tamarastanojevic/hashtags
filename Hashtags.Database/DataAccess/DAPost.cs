﻿using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DAPost
    {
        public static Post GetPostById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Post>(result);
        }

        public static List<Post> GetAllPostsFromUsersId(string id)
        {
            List<Post> returnList = new List<Post>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("OwnerId", id);
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<Post>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static List<Post> GetAllPostsFromUsersUsername(string username)
        {
            List<Post> returnList = new List<Post>();

            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("OwnerUsername", username);
            var result = collection.Find<BsonDocument>(query).ToList<BsonDocument>();

            foreach (var el in result)
            {
                returnList.Add(BsonSerializer.Deserialize<Post>(el.ToBsonDocument()));
            }

            return returnList;
        }

        public static List<Hashtag> GetAllPostHashtags(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var post = BsonSerializer.Deserialize<Post>(result.ToBsonDocument());

            return post.Hashtags;
        }

        public static List<Comment> GetAllPostComments(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var post = BsonSerializer.Deserialize<Post>(result.ToBsonDocument());

            return post.Comments;
        }

        public static List<Like> GetAllPostLikes(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var post = BsonSerializer.Deserialize<Post>(result.ToBsonDocument());

            return post.Likes;
        }

        public static Picture GetPostPicture(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var post = BsonSerializer.Deserialize<Post>(result.ToBsonDocument());

            Picture picture = new Picture();
            picture.Data = post.Attachment.Data;
            picture.Id = post.Attachment.Id;
            picture.OwnerId = post.Attachment.OwnerId;
            picture.OwnerType = post.Attachment.OwnerType;
            picture.Timestamp = post.Attachment.Timestamp;
            picture.Type = post.Attachment.Type;

            return picture;
        }

        public static void CreatePost(ObjectId ownerId, string ownerUsername, string body, string link, string editstate, List<Hashtag> hashtags)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            Post post = new Post()
            {
                OwnerId = ownerId,
                OwnerUsername = ownerUsername,
                Body = body,
                Hashtags = hashtags,
                Link = link,
                EditState = editstate
            };

            var item = post.ToBsonDocument<Post>();
            collection.InsertOne(item);
        }

        public static void UpdatePostBodyById(string id, string body)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));

            var update = Builders<BsonDocument>.Update.Set("Body", body);
            var result = collection.UpdateOneAsync(filter, update);
        }

        public static void UpdatePostLinkById(string id, string link)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));

            var update = Builders<BsonDocument>.Update.Set("Link", link);
            var result = collection.UpdateOneAsync(filter, update);
        }

        public static void UpdatePostEditStateById(string id, string editState)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));

            var update = Builders<BsonDocument>.Update.Set("EditState", editState);
            var result = collection.UpdateOneAsync(filter, update);
        }

        public static void UpdatePostHashtagsById(string id, List<Hashtag> hashtags)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));

            var update = Builders<BsonDocument>.Update.Set("Hashtags", hashtags);
            var result = collection.UpdateOneAsync(filter, update);
        }

        public static void DeletePostById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.POST_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Post post)
        {
            return post.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Post post)
        {
            return post.Timestamp.AsDateTime;
        }

        public static DateTime GetDateTime(Attachment post)
        {
            return post.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
