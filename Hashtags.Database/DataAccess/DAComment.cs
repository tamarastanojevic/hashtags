﻿using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DAComment
    {
        public static Comment GetCommentById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.COMMENT_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Comment>(result);
        }

        public static Picture GetCommentPicture(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.COMMENT_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var comment = BsonSerializer.Deserialize<Comment>(result.ToBsonDocument());

            Picture picture = new Picture();
            picture.Data = comment.Attachment.Data;
            picture.Id = comment.Attachment.Id;
            picture.OwnerId = comment.Attachment.OwnerId;
            picture.OwnerType = comment.Attachment.OwnerType;
            picture.Timestamp = comment.Attachment.Timestamp;
            picture.Type = comment.Attachment.Type;

            return picture;
        }

        public static void CreateComment(string ownerId, string ownerUsername, string text)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.COMMENT_COLLECTION);

            Comment comment = new Comment()
            {
                OwnerId = new ObjectId(ownerId),
                OwnerUsername = ownerUsername,
                Text = text
            };

            var item = comment.ToBsonDocument<Comment>();
            collection.InsertOne(item);
        }

        public static void DeleteCommentById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.COMMENT_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Comment comm)
        {
            return comm.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Comment comm)
        {
            return comm.Timestamp.AsDateTime;
        }

        public static DateTime GetDateTime(Attachment comm)
        {
            return comm.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
