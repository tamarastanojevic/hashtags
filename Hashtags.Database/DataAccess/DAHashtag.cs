﻿using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DAHashtag
    {
        public static Hashtag GetHashtagById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.HASHTAG_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Hashtag>(result);
        }

        public static List<Post> GetAllHashtagPosts(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.HASHTAG_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var hashtag = BsonSerializer.Deserialize<Hashtag>(result.ToBsonDocument());
            
            return hashtag.Posts;
        }

        public static List<User> GetAllHashtagFollowingUsers(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.HASHTAG_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            var hashtag = BsonSerializer.Deserialize<Hashtag>(result.ToBsonDocument());

            return hashtag.FollowingUsers;
        }
        public static void CreateHashtag(string name, List<Post> posts)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.HASHTAG_COLLECTION);

            Hashtag hashtag = new Hashtag()
            {
                Name = name,
                Posts = posts
            };

            var item = hashtag.ToBsonDocument<Hashtag>();
            collection.InsertOne(item);
        }

        public static void DeleteHashtagById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.HASHTAG_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Hashtag hash)
        {
            return hash.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Hashtag hash)
        {
            return hash.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
