﻿using Hashtags.Database.Enums;
using Hashtags.Database.Helpers;
using Hashtags.Database.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashtags.Database.DataAccess
{
    public class DAPicture
    {
        public static Picture GetPictureById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            var query = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.Find<BsonDocument>(query).FirstOrDefault().AsBsonDocument;
            result = result.ToBsonDocument();

            return BsonSerializer.Deserialize<Picture>(result);
        }

        public static void CreatePicture(string ownerId, OwnerType ownerType, byte[] data)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            Picture pic = new Picture()
            {
                OwnerId = new ObjectId(ownerId),
                Data = data,
                OwnerType = ownerType,
                Type= Enums.AttachmentType.Picture
            };

            var item = pic.ToBsonDocument<Picture>();
            collection.InsertOne(item);
        }

        public static void DeletePictureById(string id)
        {
            var collection = MongoContext.Instance.database.GetCollection<BsonDocument>(ConnectionHelper.ATTACHMENT_COLLECTION);

            var filter = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId(id));
            var result = collection.DeleteOne(filter);
        }

        public static string GetStringId(Picture pic)
        {
            return pic.Id.ToString();
        }

        public static ObjectId GetObjectId(string id)
        {
            return new ObjectId(id);
        }

        public static DateTime GetDateTime(Picture pic)
        {
            return pic.Timestamp.AsDateTime;
        }

        public static BsonDateTime GetBsonDateTime(DateTime date)
        {
            return new BsonDateTime(date);
        }
    }
}
